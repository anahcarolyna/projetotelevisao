package br.com.itau;

import javax.swing.*;

public class ControleRemoto {
   private int volumeMaximo = 100;
   private int canalMaximo=50;

   Televisao tv = new Televisao();

   public void aumentarDiminuirVolume(boolean aumentar){
     if(tv.isLigado()) {
        int volume = tv.getVolume();
        if (aumentar) {
           if (volume < volumeMaximo) {
              volume++;
              tv.setVolume(volume);
           } else {
              JOptionPane.showMessageDialog(null, "Volume já está no máximo!", "AVISO", JOptionPane.INFORMATION_MESSAGE);
           }
        } else {
           volume--;
           tv.setVolume(volume);
        }
     }else{
        JOptionPane.showMessageDialog(null, "A TV está desligada!", "AVISO", JOptionPane.WARNING_MESSAGE);
     }
   }

   public void incrementarCanal(boolean incrementar){
      if(tv.isLigado()) {
         int canal = tv.getCanal();
         if (incrementar) {
            if (canal < canalMaximo) {
               canal++;
               tv.setCanal(canal);
            } else {
               JOptionPane.showMessageDialog(null, "Não há mais canais disponiveis!", "Aviso",JOptionPane.INFORMATION_MESSAGE);
            }
         } else {
            canal--;
            tv.setCanal(canal);
         }
      }
      else{
         JOptionPane.showMessageDialog(null, "A TV está desligada!", "AVISO", JOptionPane.WARNING_MESSAGE);
      }
   }

   public void LigarDesligarTv(boolean ligar){
      if(ligar) {
         if (tv.isLigado()) {
            JOptionPane.showMessageDialog(null, "A televisão já está ligada!", "AVISO", JOptionPane.WARNING_MESSAGE);
         } else {
            tv.setLigado(true);
            JOptionPane.showMessageDialog(null, "TV ligada!", "Aviso",JOptionPane.INFORMATION_MESSAGE);

         }
      }
      else{
         if (!tv.isLigado()) {
            JOptionPane.showMessageDialog(null, "A televisão já está desligada!", "AVISO", JOptionPane.WARNING_MESSAGE);
         }else {
            tv.setLigado(false);
            JOptionPane.showMessageDialog(null, "TV desligada!", "Aviso",JOptionPane.INFORMATION_MESSAGE);
         }
      }
   }

   public String mostrarCanalVolumeAtual(){
      String retorno = "";
      if(tv.isLigado()) {
         retorno = "Status: Ligada"  + "\nCanal atual: " + tv.getCanal() + "\n Volume: " + tv.getVolume();
      }else{
         retorno = "Status: Desligada";
      }
      return retorno;
   }

   public void escolherCanal(int numero) {
      if (tv.isLigado()) {
         tv.setCanal(numero);
      }
      else{
         JOptionPane.showMessageDialog(null, "A TV está desligada!", "AVISO", JOptionPane.WARNING_MESSAGE);
      }
   }

   public String[] listarCanais(){
      String listaCanais[] = new String[40];

      for(int contador = 0; contador < listaCanais.length;contador+=4){
         listaCanais[contador] = Integer.toString(contador);
      }
      return listaCanais;
   }

}
