package br.com.itau;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        JDialog.setDefaultLookAndFeelDecorated(true);
        ControleRemoto controle = new ControleRemoto();
        Object[] options;
        Object selecao;
        int opcaoSelecionada;

        do {
             opcaoSelecionada = Integer.parseInt(
                    JOptionPane.showInputDialog(null, "Controle: " +
                            "\n1- Ligar TV" +
                            "\n2- Volume" +
                            "\n3- Canal" +
                            "\n4- Mostrar canal e volume" +
                            "\n5- Desligar TV" +
                            "\nEscolha uma opção."));

            switch (opcaoSelecionada) {
                case 1:
                    controle.LigarDesligarTv(true);
                    break;
                case 2:
                        options = new Object[]{"Aumentar", "Diminuir"};
                        selecao = JOptionPane.showInputDialog(null, "Selecione a opção desejada:",
                                "VOLUME", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                        if (selecao.equals("Aumentar")) {
                            controle.aumentarDiminuirVolume(true);

                        } else if (selecao.equals("Diminuir")) {
                            controle.aumentarDiminuirVolume(false);
                        }
                    break;
                case 3:
                    options = new Object[]{"Aumentar", "Diminuir", "Selecionar"};
                    selecao = JOptionPane.showInputDialog(null, "Canal:",
                            "CANAL", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                    if (selecao.equals("Aumentar")) {
                        controle.incrementarCanal(true);
                    } else if (selecao.equals("Diminuir")) {
                       controle.incrementarCanal(false);
                    }else if(selecao.equals("Selecionar")){
                        controle.escolherCanal(Integer.parseInt(JOptionPane.showInputDialog(null, "Escolha um dos canais disponíveis:", "Input", JOptionPane.QUESTION_MESSAGE,
                                null, controle.listarCanais(),0).toString()));
                    }
                    break;
                case 4:
                    JOptionPane.showMessageDialog(null,  controle.mostrarCanalVolumeAtual(), "AVISO", JOptionPane.WARNING_MESSAGE);
                    break;
                case 5:
                    controle.LigarDesligarTv(false);
                    System.exit(0);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida! Escolha uma opção válida no controle");
                    break;
            }
        } while (opcaoSelecionada >=1 && opcaoSelecionada <= 5);
    }
}
